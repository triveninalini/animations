export default function reducer(state = {
  blog_1: {
    title: "Blog 1",
    content: "Quis consequat quis dolore cillum pariatur cupidatat sit do non irure do laboris. Elit cupidatat cillum sunt amet in. Aliquip consequat qui laboris commodo Lorem ea velit. Consequat deserunt deserunt anim magna cupidatat reprehenderit. Proident veniam magna aliquip labore deserunt sint officia non adipisicing est dolor. Eu velit labore duis veniam ipsum ullamco tempor aliqua cillum ullamco culpa id dolore. Aliquip do nulla amet sunt incididunt eu dolor mollit dolor anim sunt dolore consectetur incididunt."
  },
  blog_2: {
    title: "Blog 2",
    content: "Irure officia nulla sunt cupidatat ea elit velit ipsum anim occaecat do nulla tempor. Nostrud nisi id mollit veniam. Officia minim cupidatat in excepteur irure ex officia laboris consequat ullamco. Nostrud magna do aliqua sint laborum pariatur."
  }
}, action) {
  
  switch(action.type) {
    case("GET_BLOGS"): return {
      ...state
    }
    default: return state;
  }
}