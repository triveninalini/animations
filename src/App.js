import React, { useState, Fragment, useEffect } from 'react';
import './App.css';
import Hexagon from './components/polygon/hexagon';
import Cube from "./components/polygon/cube";
import Rectangle from './components/rectangle/rectangle';
import WavyCircle from './components/wavyCircle';
import BoxShadow from "./components/box-shadow"
import RainAnimation from './components/rainAnimation';
import Split from './components/effects/splitBackground';
import Fluid from './components/effects/fluidEffects';
import Product from './components/productCard';
import Checkbox from './components/checkbox';
import Web from "./components/demo";
import Range from "./components/range";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { useSelector } from 'react-redux';


function App() {

  const ref = React.createRef();
  const [toggle, set_toggle] = useState(false);



  useEffect(() => {
    function onScroll() {
      try {
        if (ref.current)
          ref.current.classList.toggle("sticky", window.scrollY > 100);
        else 
          document.getElementById("header").classList.toggle("sticky", window.scrollY > 100);
      } catch (ex) {
        console.error(`error in scrolling`, ref);
      }
    }
    window.addEventListener("scroll", onScroll);
    return () => {
      window.removeEventListener("scroll", onScroll);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const blogs = useSelector(store => store.blogs);
  return (
    <Router>
      <div className="web-container" >
        <header id="header" className={toggle && "active"} ref={ref}>
          <ul>
            <li><a href={`/#home`} onClick={(e) => { set_toggle(!toggle) }}>Home</a></li>
            <li><a href={`/#services`} onClick={() => set_toggle(!toggle)}>Services</a></li>
            <li><a href={`/#about`} onClick={() => set_toggle(!toggle)}>About</a></li>
            <li><a href={`/#skills`} onClick={() => set_toggle(!toggle)}>skills</a></li>
            <li><a href={`/#contact`} onClick={() => set_toggle(!toggle)}>contact</a></li>
          </ul>
          <div className="toggle" onClick={() => set_toggle(!toggle)}></div>
        </header>
      </div>
      <Switch>
        <Route path="/" exact ><Web /></Route>
      </Switch>
    </Router>
  );
}

export default App;
