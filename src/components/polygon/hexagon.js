import React, { Fragment } from "react";
import "./style.css";

function Hexagon() {
  
  const cursorRef = React.createRef();
  const hexagons = [];
  for (let i = 0; i < 32; i++) {
    hexagons.push(i);
  }

  function mouseMove(e) {
    cursorRef.current.style.left = e.clientX + "px";
    cursorRef.current.style.top = e.clientY + "px";
  }

  return <Fragment><div className="container" onMouseMove = {(e) => mouseMove(e)}>
    {
      hexagons.map(hexagon =>
        (<div className="row">
          {hexagons.map(item => (<div className="hexagon"></div>))}
        </div>
        ))}
  </div>
  <div className = "cursor" ref = {cursorRef}></div>
  </Fragment>

}

export default Hexagon;