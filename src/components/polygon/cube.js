import React from "react";
import "./style.css";

function Cube() {
  const cube = [];
  for (let i = 0; i < 10; i++) {
    cube.push(i);
  }

  return (<div className="cube-container">
    {cube.map(item => <div className="cube-row">
       {cube.map(item => <div className = "cube">
          {[1,2,3].map(cube => <span></span>)}
      </div>)}
    </div>)}
  </div>)
}

export default Cube;