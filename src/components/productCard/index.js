import React from "react";
import "./style.css";
import shoes from "../../images/shoes.png"

function Product() {


  return <div className="product-container">
    <div className="card">
      <div className="card-img">
        <img src={shoes} alt=""/>
        </div>
        <div className = "content-box">
          <h2>Nike Shoes</h2>
          <div className = "size">
            <h3>Size  : </h3>
            <span>7</span>
            <span>8</span>
            <span>9</span>
            <span>10</span>
          </div>
          <div className = "color">
            <h3>Color :   </h3>
            <span style = {{backgroundColor: "red"}}></span>
            <span style = {{backgroundColor: "yellow"}}></span>
            <span style = {{backgroundColor: "green"}}></span>
          </div>
          <button type = "button" href = "/">Buy Now</button>
        </div>
      </div>

    </div>
}

export default Product;