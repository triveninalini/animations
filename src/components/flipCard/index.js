import React, { Fragment } from "react";

function App() {

  return (
    <Fragment>
      <div className="scene">
        <div className="card">
          <div className="card-child front">
            front
      </div>
          <div className="card-child back" >back</div>
        </div>
      </div >
      <div className="perspective">
        hello world
    </div>
    </Fragment>
  );
}
export default App;