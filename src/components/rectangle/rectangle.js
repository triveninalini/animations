import React from "react";
import "./style.css";
function Rectangle() {

  const hexagons = [];
  for (let i = 0; i < 50; i++) {
    hexagons.push(i);
  }
  return  <div className = "rectangle-container">{hexagons.map((item, index) => (<div className="rectangle"></div>))}</div>;
}

export default Rectangle;