import React from "react";
import "./style.css";

function Range(props) {
  return <div className = "skill">
    <h4>{props.title}({props.width})</h4>
    <div className="range-container">
    <div className="range" style={{ width: props.width }}></div>
  </div>
  </div>
}

export default Range;