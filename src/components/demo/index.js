import React, { useState } from "react";
import logo from "../../images/logo192.png";
import "./style.css";
import dp from "../../images/dp.jpeg";
import axios from "axios";
import Range from "../range";
import web from "../../images/web.png";
import Popup from "../popup";

function Web() {
  const skillSet = [{
    title: "JS",
    width: "80%"
  },
  {
    title: "HTML/CSS",
    width: "90%"
  },
  {
    title: "ReactJS",
    width: "90%"
  },
  {
    title: "NodeJS",
    width: "70%"
  },
  {
    title: "MongoDB",
    width: "70%"
  }]

  const fields = {
    value: "",
    error: false
  }

  const [name, set_name] = useState({ ...fields });
  const [email, set_email] = useState({ ...fields });
  const [details, set_details] = useState({ ...fields });
  const [show_popup, set_popup] = useState(false);
  const [error_message, set_error_message] = useState("")

  function isDetailsValid(value = details.value || "") {
    return value.length > 20;
  }

  function isNameValid(value = name.value) {
    return !!value;
  }

  function isEmailValid(value = email.value) {
    return !!value;
  }

  function onNameChange(value) {
    if (isNameValid(value)) {
      name.error = false;
    } else {
      name.error = true;
    }
    set_name({ ...name, value })
  }

  function onEmailChange(value) {
    if (isEmailValid(value)) {
      email.error = false;
    } else {
      email.error = true;
    }
    set_email({ ...email, value })
  }


  function onDetailsChange(value) {
    if (isDetailsValid()) {
      details.error = false;
    } else {
      details.error = true;
    }
    set_details({ ...details, value });
  }

  function isValid() {
    try {
      let valid = true;
      if (!isNameValid()) {
        name.error = true;
        valid = false;
        set_name({ ...name });
      }
      if (!isEmailValid()) {
        valid = false;
        email.error = true;
        set_email({ ...email });
      }
      if (!isDetailsValid()) {
        details.error = true;
        valid = false;
        set_details({ ...details });
      }
      return valid;
    } catch (ex) {
      console.error(`invalid inputs`);
    }
  }
  function sendMessage(e) {
    e.preventDefault();
    try {
      if (isValid()) {
        const data = {
          email: `${email.value}\nname: ${name.value}`,
          subject: "new client",
          content: details.value
        };
        axios.post("https://my-protofolio-server.herokuapp.com/mailer/sendEmail", data, {
          headers: {
            contentType: "application/json"
          }
        })
          .then(response => {
            console.log(response);
            set_error_message("");
            set_popup(true);
          }).catch(ex => {
            set_popup(true);
            set_error_message("Something Went wrong")
            console.error("error in sending mail", ex)
          });
      } else {
        set_error_message("invalid input")
      }
    } catch (ex) {
      console.error(`error in sending mail`, ex);
      throw ex;
    }
  }


  return <div className="web-container" >
    {show_popup ? <Popup show_popup={set_popup} content={!error_message ? "Message sent succesfully": error_message} error={!!error_message} /> : null}
    <section className="banner" id="home">
      <h1><strong><span>TRIVENI K</span></strong>
        <p>Web Developer, Bangalore</p>
      </h1>
    </section>
    <section className="sec" id="about">
      <div className="content">
        <div className="mxw800p">
        </div>
        <div className="author">

          <div className="author-img"><img src={dp} /></div>
          <div className="content">
            <h3>MERN Stack Developer</h3>
            <p>Hello, I am Triveni, a MERN stack developer with 2+ years of experience.My eagerness to explore technologies, keeps me running in the buisness.

            You can count on me, when you need quality product using MERN technologies.

            I believe in building Excellent UX, High Performance and Easy to Maintain websites.

            Test cases and thoughtful code reviews are must in my web development process.</p>
          </div>
        </div>

      </div>
    </section>
    <section className="sec" id="services">
      <div className="content">
        <div className="mxw800p">
          <h3>What We Do</h3>
          <p>We develop a static and dynamic websites with Excellent User Interface as per the client requirements. Quality test cases using JEST, Mocha and chai ensured for good maintainence.</p>
        </div>
        <div className="services">
          <div className="box">
            <div className="icon">
              <img src={logo} alt="react" />
            </div>
            <div className="content">
              <h2>UI Development</h2>
              <p>Develop an Excellent User Interface and Integrate the features with backend.</p>
            </div>
          </div>
          <div className="box">
            <div className="icon">
              <img src={web} alt="web" />
            </div>
            <div className="content">
              <h2>Back End Development</h2>
              <p>Develop backend using micorservice architechture with quality unit test cases using nodejs, mocha and chai.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="sec work" id="skills">
      <div className="content">
        <div className="mxw800p">
          <h3>Skills</h3>
        </div>
        <div className="skills">
          {skillSet.map(item => <Range {...item} />)}
        </div>
      </div>
    </section>
    <section className="sec" id="contact">
      <div className="content">
        <div className="mxw800p">
          <h3>Send Us a Message</h3>
          <p>Get in touch with me</p>
        </div>
        <div className="contact-form">
          <form>
            {error_message && <div className="row100">
              <div className="inputBx50">
                <p className="error">Invalid input</p>
              </div>
            </div>}
            <div className="row100">
              <div className="inputBx50">
                <input placeholder="Full name" type="text" onChange={(e) => onNameChange(e.target.value)} />
              </div>
            </div>
            <div className="row100">
              <div className="inputBx50">
                <input placeholder="E-mail" type="text" onChange={(e) => onEmailChange(e.target.value)} />
              </div>
            </div>
            <div className="row100">
              <div className="inputBx50">
                <textarea placeholder="Message" type="text" onChange={(e) => onDetailsChange(e.target.value)} />
              </div>
            </div>

            <div className="row100">
              <div className="inputBx50">
                <input placeholder="Send" type="submit" onClick={(e) => sendMessage(e)} />
              </div>
            </div>
          </form>
        </div>
      </div>

    </section>

  </div>
}

export default Web;