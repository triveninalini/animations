import React, {   Fragment, useEffect } from "react";
import ReactDOM from 'react-dom';
import "./style.css"

function Split() {
  const ref = React.createRef();
  function onScroll() {
    console.log(ref);
    ref.current.children[0].style.transform = "translateX(-1000px)";
    ref.current.children[1].style.transform = "translateX(1000px)";
  }

  return (<section className = "fluid-section" ref = {ref} onClick = {onScroll}><div className="side" id="side1">hi</div>
    <div className="side" id="side2">hi</div>
    </section>);
}

export default Split;