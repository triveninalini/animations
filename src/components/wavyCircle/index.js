import React from "react";
import "./style.css";

function WavyCircle() {

  const hexagons = [], length = 15;

  for (let i = 0; i < length; i++) {
    hexagons.push(i);
  }
  return (<div className="loader-container">
    <div className="loader">
      {hexagons.map((item, index) => {
        const value = (index * 10) + "px";
        const style  = { top: value, bottom: value, left: value, right: value } 
        return (<span style={style}></span>)
      })}
    </div>
  </div>)
}

export default WavyCircle;