import React, { useState } from "react";
import "./style.css";

function Popup(props) {

  useState(() => {
    setTimeout(() => { props.show_popup(false) }, 3000);
  }, [])

  const class_name = props.error ? "popup error" : "popup";
  return (<div className="popup-container">
    <div className={class_name}>{props.content}</div>
  </div >);
}

export default Popup;