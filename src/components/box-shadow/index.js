import React from "react";
import "./style.css";

function BoxShadow() {
  return <div className="box-container">
    <div className="box shadow1">Shadow 1</div>
    <div className="box shadow2">Shadow 2</div>
    <div className="box shadow3">Shadow 3</div>
    <div className="box shadow4">Shadow 4</div>
    <div className="box shadow5">Shadow 5</div>
    <div className="box shadow6">Shadow 6</div>
    <div className="box shadow7">Shadow 7</div>
  </div>
}

export default BoxShadow;